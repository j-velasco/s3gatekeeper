test:
	go test ./...

lint:
	go fmt ./...
	go vet ./...

bin:
	go build -o s3gatekeeper-server ./cmd/server

image:
	docker build \
	  --build-arg BUILD_DATE="`gdate --rfc-3339=seconds -u`" \
	  --build-arg VCS_REF=`git rev-parse HEAD` \
	  -t s3gatekeeper \
	  .

update-minio-image: minio-image
	docker tag miniotestci jvelasco/s3gatekeeper-miniotestci
	docker push jvelasco/s3gatekeeper-miniotestci

minio-image:
	docker build -t miniotestci -f Dockerfile.miniocitest .

start-minio:
	docker run -p 9000:9000 -d --name oauths3bucket-minio miniotestci

stop-minio:
	docker rm -f oauths3bucket-minio