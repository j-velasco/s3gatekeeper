package main

import (
	"net/http"

	"net/url"

	awsSession "github.com/aws/aws-sdk-go/aws/session"

	"os"

	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.com/j-velasco/s3gatekeeper/pkg"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/s3proxy"

	_ "github.com/joho/godotenv/autoload"
)

func NewHandler(ss sessions.Store, exSrv pkg.OAuthExchangeCodeService, conf *httphandler.Oauth2Config) http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/oauth/callback", httphandler.NewOAuthCallbackHandler(ss, exSrv))
	logoutURL, _ := url.Parse("/oauth/logout")
	emailMatcher, err := httphandler.NewEmailMatcherGuardMiddleware(
		os.Getenv("EMAIL_REGEXP_MATCH"),
		logoutURL,
		ss,
		conf,
	)
	if err != nil {
		panic(err)
	}
	sess, err := awsSession.NewSession()
	if err != nil {
		panic(err)
	}
	s3Client := s3.New(sess)

	r.NotFoundHandler = negroni.New(
		negroni.HandlerFunc(httphandler.NewUserInSessionGuardMiddleware(ss, conf)),
		negroni.HandlerFunc(emailMatcher),
		negroni.Wrap(http.HandlerFunc(s3proxy.NewS3Proxy(s3Client, os.Getenv("BUCKET"), os.Getenv("PREFIX")))),
	)
	n := negroni.Classic()
	n.UseHandler(r)

	return r
}
