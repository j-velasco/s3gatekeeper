package session

import (
	"net/http"

	"github.com/gorilla/sessions"
)

type inMemoryGorillaSessionStore struct {
	memory map[string]*sessions.Session
}

func NewInMemoryGorillaSessionStore() *inMemoryGorillaSessionStore {
	return &inMemoryGorillaSessionStore{memory: make(map[string]*sessions.Session)}
}

func (s *inMemoryGorillaSessionStore) Get(r *http.Request, name string) (*sessions.Session, error) {
	ss, ok := s.memory[name]
	if !ok {
		return sessions.NewSession(s, name), nil
	}

	return ss, nil
}

func (s *inMemoryGorillaSessionStore) New(r *http.Request, name string) (*sessions.Session, error) {
	return sessions.NewSession(s, name), nil
}

func (s *inMemoryGorillaSessionStore) Save(r *http.Request, w http.ResponseWriter, ss *sessions.Session) error {
	s.memory[ss.Name()] = ss
	return nil
}
