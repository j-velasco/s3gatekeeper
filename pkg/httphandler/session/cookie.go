package session

import "github.com/gorilla/sessions"

func NewStore(secret string) *sessions.CookieStore {
	store := sessions.NewCookieStore([]byte(secret))
	store.MaxAge(60 * 60) // 1h
	return store
}
