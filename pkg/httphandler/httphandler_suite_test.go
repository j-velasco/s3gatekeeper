package httphandler_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestHttphandler(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Httphandler Suite")
}
