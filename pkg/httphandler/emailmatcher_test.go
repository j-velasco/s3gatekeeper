package httphandler_test

import (
	"net/http"
	"net/http/httptest"

	"net/url"

	"io/ioutil"

	"context"

	"github.com/gorilla/sessions"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler/session"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/mock"
	"golang.org/x/oauth2"
)

var _ = Describe("Pkg/Httphandler/Emailmatcher", func() {
	const atTypeformRegexp = ".*@typeform.com"

	var (
		logoutUrl  *url.URL
		ss         sessions.Store
		middleware httphandler.Middleware
		r          *http.Request
		rr         *httptest.ResponseRecorder
		conf       *httphandler.Oauth2Config
	)

	BeforeEach(func() {
		logoutUrl, _ = url.Parse("/oauth/logout")
		ss = session.NewInMemoryGorillaSessionStore()
		conf = &httphandler.Oauth2Config{
			Config: &oauth2.Config{
				ClientID:     clientId,
				ClientSecret: clientSecret,
				RedirectURL:  redirectURL,
				Scopes:       []string{"openid", "profile"},
				Endpoint: oauth2.Endpoint{
					AuthURL:  authorizeURL,
					TokenURL: tokenURL,
				},
			},
			Audience: audience,
		}

		middleware, _ = httphandler.NewEmailMatcherGuardMiddleware(
			atTypeformRegexp,
			logoutUrl,
			ss,
			conf,
		)
		rr = httptest.NewRecorder()
		r = httptest.NewRequest(http.MethodGet, "/", nil)
	})

	It("shows logout page when user hasn't give consent", func() {
		hasNextBeenCalled := false
		next := func(w http.ResponseWriter, r *http.Request) {
			hasNextBeenCalled = true
		}
		middleware(rr, r, next)

		Expect(hasNextBeenCalled).To(BeFalse())
		Expect(rr.Result().StatusCode).To(Equal(http.StatusBadRequest))
		b, err := ioutil.ReadAll(rr.Result().Body)
		Expect(err).ToNot(HaveOccurred())
		Expect(b).To(MatchRegexp(`<a href="/oauth/logout">`))
	})

	It("calls next when email matches regexp", func() {
		u := mock.NewUserStub("jorge@typeform.com")
		r = r.WithContext(context.WithValue(r.Context(), "user", u))

		hasNextBeenCalled := false
		next := func(w http.ResponseWriter, r *http.Request) {
			hasNextBeenCalled = true
		}
		middleware(rr, r, next)

		Expect(hasNextBeenCalled).To(BeTrue())
	})

	It("redirects to consent screen when email doesn't match regexp", func() {
		u := mock.NewUserStub("jorge@other.com")
		r = r.WithContext(context.WithValue(r.Context(), "user", u))
		hasNextBeenCalled := false
		next := func(w http.ResponseWriter, r *http.Request) {
			hasNextBeenCalled = true
		}
		middleware(rr, r, next)

		Expect(hasNextBeenCalled).To(BeFalse())
		expectRedirectionToConsentScreen(rr, r, ss)
	})
})
