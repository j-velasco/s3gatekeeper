package httphandler_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"net/http"
	"net/http/httptest"
	"net/url"

	"github.com/gorilla/sessions"
	"gitlab.com/j-velasco/s3gatekeeper/pkg"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler/session"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/mock"
	"golang.org/x/oauth2"
)

const (
	audience     = "https://audience.org/api/v2/"
	clientId     = "CLIENT_ID"
	clientSecret = "CLIENT_SECRET"
	redirectURL  = "http://redirect.org/callback"
	authorizeURL = "https://auth.provider.org/authorize"
	tokenURL     = "https://auth.provider.org/oauth/token"
)

var _ = Describe("Httphandler/OAuth", func() {
	var ss sessions.Store
	BeforeEach(func() {
		ss = session.NewInMemoryGorillaSessionStore()
	})

	Describe("User in session guard", func() {
		var (
			conf       *httphandler.Oauth2Config
			middleware httphandler.Middleware
		)

		BeforeEach(func() {
			conf = &httphandler.Oauth2Config{
				Config: &oauth2.Config{
					ClientID:     clientId,
					ClientSecret: clientSecret,
					RedirectURL:  redirectURL,
					Scopes:       []string{"openid", "profile"},
					Endpoint: oauth2.Endpoint{
						AuthURL:  authorizeURL,
						TokenURL: tokenURL,
					},
				},
				Audience: audience,
			}
			middleware = httphandler.NewUserInSessionGuardMiddleware(ss, conf)
		})

		Context("No user present in session", func() {
			It("redirects to OAuth2 screen", func() {
				rr := httptest.NewRecorder()
				hasNextBeenCalled := false
				next := func(w http.ResponseWriter, r *http.Request) {
					hasNextBeenCalled = true
				}
				r, _ := http.NewRequest(http.MethodGet, "/", nil)
				middleware(rr, r, next)

				Expect(hasNextBeenCalled).To(BeFalse())

				expectRedirectionToConsentScreen(rr, r, ss)
			})

			It("remembers requested document", func() {
				rr := httptest.NewRecorder()
				next := func(w http.ResponseWriter, r *http.Request) {}
				r, _ := http.NewRequest(http.MethodGet, "/requested/document", nil)
				middleware(rr, r, next)

				s, err := ss.Get(r, "oauth")
				Expect(err).ShouldNot(HaveOccurred())

				Expect(s.Values["target_path"]).To(Equal("/requested/document"))
			})
		})

		Context("User is present in session", func() {
			It("calls next", func() {
				rr := httptest.NewRecorder()
				hasNextBeenCalled := false
				var requestToNext *http.Request
				next := func(w http.ResponseWriter, r *http.Request) {
					hasNextBeenCalled = true
					requestToNext = r
				}
				r, err := http.NewRequest(http.MethodGet, "/", nil)
				Expect(err).ToNot(HaveOccurred())
				s, err := ss.Get(r, "oauth")
				Expect(err).ToNot(HaveOccurred())
				u := mock.NewUserStub("jhon@doe.com")
				s.Values["user"] = u
				err = ss.Save(r, rr, s)
				Expect(err).ToNot(HaveOccurred())

				middleware(rr, r, next)

				Expect(hasNextBeenCalled).To(BeTrue())
				user, ok := requestToNext.Context().Value("user").(pkg.User)
				Expect(ok).To(BeTrue())
				Expect(user.Email()).To(Equal("jhon@doe.com"))
			})
		})
	})

	Describe("OAuth callback handler", func() {
		var ss sessions.Store
		BeforeEach(func() {
			ss = session.NewInMemoryGorillaSessionStore()
		})

		It("validates state", func() {
			exSrv := mock.NewExchangeServiceStub()
			middleware := httphandler.NewOAuthCallbackHandler(ss, exSrv)

			rr := httptest.NewRecorder()
			r, _ := http.NewRequest(http.MethodGet, "/callback?state=different-state", nil)

			setStateInSession(ss, r, rr)
			middleware(rr, r)

			Expect(rr.Result().StatusCode).To(Equal(http.StatusBadRequest))
		})

		It("exchanges code for user", func() {
			fs := mock.NewExchangeServiceStub()
			middleware := httphandler.NewOAuthCallbackHandler(ss, fs)

			rr := httptest.NewRecorder()
			r, _ := http.NewRequest(http.MethodGet, "/callback?state=state&code=OAUTH_CODE", nil)

			setStateInSession(ss, r, rr)
			u := mock.NewUserStub("jhon@doe.com")
			fs.WhenCode(pkg.OAuthCode("OAUTH_CODE"), u, nil)

			middleware(rr, r)

			Expect(rr.Result().StatusCode).To(Equal(http.StatusFound))
			Expect(rr.Result().Header.Get("Location")).To(Equal("/"))
			s, err := ss.Get(r, "oauth")
			Expect(err).ToNot(HaveOccurred())
			userFromSession, ok := s.Values["user"].(pkg.User)
			Expect(ok).To(BeTrue())
			Expect(userFromSession.Email()).To(Equal("jhon@doe.com"))
		})

		It("redirects to requested document", func() {
			fs := mock.NewExchangeServiceStub()
			middleware := httphandler.NewOAuthCallbackHandler(ss, fs)

			rr := httptest.NewRecorder()
			r, _ := http.NewRequest(http.MethodGet, "/callback?state=state&code=OAUTH_CODE", nil)

			setStateInSession(ss, r, rr)
			setTargetPathInSession("/requested/document", ss, r, rr)
			u := mock.NewUserStub("jhon@doe.com")
			fs.WhenCode(pkg.OAuthCode("OAUTH_CODE"), u, nil)

			middleware(rr, r)

			Expect(rr.Result().StatusCode).To(Equal(http.StatusFound))
			Expect(rr.Result().Header.Get("Location")).To(Equal("/requested/document"))
		})
	})
})

func expectRedirectionToConsentScreen(rr *httptest.ResponseRecorder, r *http.Request, ss sessions.Store) {
	Expect(rr.Result().StatusCode).To(Equal(http.StatusTemporaryRedirect))
	redirectedTo, err := url.Parse(rr.Result().Header.Get("Location"))
	Expect(err).ToNot(HaveOccurred())
	Expect(redirectedTo.Query().Get("audience")).To(Equal(audience))
	stateContainedInRedirection := redirectedTo.Query().Get("state")
	Expect(stateContainedInRedirection).To(Not(BeEmpty()))
	s, err := ss.Get(r, "oauth")
	Expect(err).ToNot(HaveOccurred())
	Expect(s.Values["state"]).To(Equal(stateContainedInRedirection))
}

func setStateInSession(ss sessions.Store, r *http.Request, rr *httptest.ResponseRecorder) {
	s, err := ss.Get(r, "oauth")
	Expect(err).ToNot(HaveOccurred())
	s.Values["state"] = "state"
	err = ss.Save(r, rr, s)
	Expect(err).ToNot(HaveOccurred())
}

func setTargetPathInSession(path string, ss sessions.Store, r *http.Request, rr *httptest.ResponseRecorder) {
	s, err := ss.Get(r, "oauth")
	Expect(err).ToNot(HaveOccurred())
	s.Values["target_path"] = path
	err = ss.Save(r, rr, s)
	Expect(err).ToNot(HaveOccurred())
}
