package s3proxy

import (
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"

	"github.com/aws/aws-sdk-go/service/s3"
)

type S3Proxy struct {
	bucket    string
	keyPrefix string
	s3Client  *s3.S3
}

func NewS3Proxy(s3Client *s3.S3, bucket string, keyPrefix string) http.HandlerFunc {
	proxy := S3Proxy{
		s3Client:  s3Client,
		bucket:    bucket,
		keyPrefix: keyPrefix,
	}
	return proxy.handle
}

func (p *S3Proxy) handle(w http.ResponseWriter, r *http.Request) {
	logEntry := logrus.WithField("requested_path", r.URL.Path)
	err := p.serveObject(w, r)
	if err == nil {
		logEntry.Info("Object served")
		return
	}
	awsErr, _ := err.(awserr.Error)

	if awsErr.Code() == "NoSuchKey" {
		p.listDirectory(w, r, logEntry)
	} else {
		p.addAwsErrInfo(awsErr, logEntry, r.URL.Path).
			Error("Getting object")
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func (p *S3Proxy) addAwsErrInfo(awsErr awserr.Error, logEntry *logrus.Entry, path string) *logrus.Entry {
	origErr := awsErr.OrigErr()
	original := ""
	if origErr != nil {
		original = origErr.Error()
	}

	return logEntry.WithField("aws_code", awsErr.Code()).
		WithField("aws_message", awsErr.Message()).
		WithField("aws_original_error", original).
		WithField("requested_path", path)
}

func (p *S3Proxy) serveObject(w http.ResponseWriter, r *http.Request) error {
	requestedObjectKey := p.keyPrefix + r.URL.Path
	out, err := p.s3Client.GetObject(&s3.GetObjectInput{
		Bucket: &p.bucket,
		Key:    &requestedObjectKey,
	})

	if err != nil {
		return err
	}

	w.Header().Set("Content-type", *out.ContentType)
	io.Copy(w, out.Body)
	return nil
}

func (p *S3Proxy) listDirectory(w http.ResponseWriter, r *http.Request, le *logrus.Entry) {
	requestedObjectKey := p.keyPrefix + r.URL.Path

	l, err := p.s3Client.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket:    &p.bucket,
		Prefix:    aws.String(strings.TrimRight(requestedObjectKey, "/") + "/"),
		Delimiter: aws.String("/"),
	})
	if err != nil {
		le.WithField("error", err).
			Error("listing directory")
		http.Error(w, fmt.Sprintf("Directory %s cannot be retrieved", requestedObjectKey), http.StatusInternalServerError)
		return
	}
	if len(l.Contents) == 0 && len(l.CommonPrefixes) == 0 {
		le.Info("directory not found")
		http.Error(w, fmt.Sprintf("Object %s doesn't exists", requestedObjectKey), http.StatusNotFound)
		return
	}
	links := make(linkList, 0, len(l.CommonPrefixes)+len(l.Contents))
	for _, cp := range l.CommonPrefixes {
		links = append(
			links,
			newLink(strings.TrimPrefix(*cp.Prefix, p.keyPrefix), strings.TrimPrefix(*cp.Prefix, requestedObjectKey), "folder"),
		)
	}
	for _, c := range l.Contents {
		links = append(
			links,
			newLink(strings.TrimLeft(*c.Key, p.keyPrefix), strings.TrimLeft(strings.TrimPrefix(*c.Key, requestedObjectKey), "/"), "page"),
		)
	}
	responseBody := toHtml(links)
	w.Header().Set("Content-type", "text/html")
	w.Write([]byte(responseBody))
	le.Info("Directory listed")
}

func newLink(href, text, kind string) link {
	return link{href: href, text: text, kind: kind}

}

const directoryListingTemplate = `<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<style>
			body {
				background-color: #F6F9F7;
			}
			a {
				color: #1CB6DD;
			}
			a:visited {
				color: #1CB6DD;
			}
		</style>
	</head>
	<body class="mui-container-fluid">
		<ul class="mdc-list">%s<ul>
	</body>
</html>`

func toHtml(list linkList) string {
	htmlLinks := make([]string, 0, len(list))
	for _, l := range list {
		icon := ""
		if l.kind == "folder" {
			icon = "folder"
		}
		if l.kind == "page" {
			icon = "public"
		}
		htmlLinks = append(
			htmlLinks,
			fmt.Sprintf(
				`<li class="mdc-list-item"><i class="material-icons md-18">%s</i><a href="%s" class="mdc-list-item__text">%s</a></li>`,
				icon,
				l.href,
				l.text,
			),
		)
	}

	return fmt.Sprintf(directoryListingTemplate, strings.Join(htmlLinks, ""))
}

type link struct {
	href string
	text string
	kind string
}
type linkList []link
