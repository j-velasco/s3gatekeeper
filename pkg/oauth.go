package pkg

type User interface {
	Email() string
}
type OAuthCode string
type OAuthExchangeCodeService interface {
	ExchangeCode(code OAuthCode) (User, error)
}
