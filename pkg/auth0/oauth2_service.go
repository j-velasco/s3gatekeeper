package auth0

import (
	"context"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/j-velasco/s3gatekeeper/pkg"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler"
)

func init() {
	gob.Register(auth0Profile{})
}

type OauthExchangeCodeService struct {
	config *httphandler.Oauth2Config
}

type auth0Profile struct {
	UserId        string `json:"user_id"`
	Name          string `json:"name"`
	ExternalEmail string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Picture       string `json:"picture"`
	client        http.Client
}

func (p auth0Profile) Email() string {
	return p.ExternalEmail
}

func NewOAuthExchangeCodeService(c *httphandler.Oauth2Config) *OauthExchangeCodeService {
	return &OauthExchangeCodeService{config: c}
}

func (s *OauthExchangeCodeService) ExchangeCode(code pkg.OAuthCode) (pkg.User, error) {
	token, err := s.config.Exchange(context.Background(), string(code))
	if err != nil {
		return nil, errors.WithStack(err)
	}

	// Getting now the userInfo
	client := s.config.Client(context.Background(), token)
	resp, err := client.Get(s.config.UserInfo)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	profile, err := s.getAuth0Profile(resp)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	if len(profile.ExternalEmail) == 0 {
		return nil, fmt.Errorf("profile doesn't contains email")
	}

	if !profile.EmailVerified {
		return nil, fmt.Errorf("profile doesn't contains a verified email")
	}

	return profile, nil
}

func (s OauthExchangeCodeService) getAuth0Profile(resp *http.Response) (auth0Profile, error) {
	var profile auth0Profile

	raw, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if err != nil {
		return profile, err
	}

	if err = json.Unmarshal(raw, &profile); err != nil {
		return profile, err
	}

	return profile, nil
}
