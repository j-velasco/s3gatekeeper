package auth0_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"

	"gitlab.com/j-velasco/s3gatekeeper/pkg/auth0"

	"time"

	"github.com/RangelReale/osin"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/j-velasco/s3gatekeeper/pkg/httphandler"
	"golang.org/x/oauth2"
)

const existingUserWithoutTokenCode = "USER_WITHOUT_TOKEN"
const existingUserWithTokenCode = "USER_WITH_TOKEN"

var existingUserWithoutToken = &auth0User{
	Email:         "email@example.net",
	EmailVerified: true,
	Name:          "Jorge",
	UserId:        "google-oauth2|3435",
}

var existingUserWithToken = &auth0User{
	Email:         "other_email@example.net",
	EmailVerified: true,
	Name:          "Claudia",
	UserId:        "google-oauth2|abcd",
}

var _ = Describe("Testing with Ginkgo", func() {
	It("handler oauth middleware should get user from auth0 and add it to context", func() {
		ts := fakeAuth0Server()
		defer ts.Close()

		us := newOauth2Service(ts.URL)
		u, err := us.ExchangeCode(existingUserWithoutTokenCode)
		Expect(err).ShouldNot(HaveOccurred())
		Expect(u.Email()).To(Equal(existingUserWithoutToken.Email))
	})
})

func newOauth2Service(authUrl string) *auth0.OauthExchangeCodeService {
	conf := &httphandler.Oauth2Config{
		Config: &oauth2.Config{
			ClientID:     "AUTH0_CLIENT_ID",
			ClientSecret: "AUTH0_CLIENT_SECRET",
			RedirectURL:  "AUTH0_CALLBACK_URL",
			Scopes:       []string{"openid", "auth0Profile"},
			Endpoint: oauth2.Endpoint{
				AuthURL:  authUrl + "/authorize",
				TokenURL: authUrl + "/oauth/token",
			},
		},
		UserInfo: authUrl + "/userinfo",
	}

	return auth0.NewOAuthExchangeCodeService(conf)
}

func fakeAuth0Server() *httptest.Server {
	oauthServer := newOauthServer()
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodPost && r.URL.Path == "/oauth/token" {
			resp := oauthServer.NewResponse()
			defer resp.Close()

			if ar := oauthServer.HandleAccessRequest(resp, r); ar != nil {
				ar.Authorized = true
				oauthServer.FinishAccessRequest(resp, r, ar)
			}
			osin.OutputJSON(resp, w, r)
		} else if r.Method == http.MethodGet && r.URL.Path == "/userinfo" {
			resp := oauthServer.NewResponse()
			defer resp.Close()

			if ir := oauthServer.HandleInfoRequest(resp, r); ir != nil {
				// mimic Auth0 response data
				u, _ := ir.AccessData.UserData.(*auth0User)
				um, _ := json.Marshal(u)
				json.Unmarshal(um, &resp.Output)
				oauthServer.FinishInfoRequest(resp, r, ir)
			}
			osin.OutputJSON(resp, w, r)
		} else {
			w.WriteHeader(http.StatusBadRequest)
		}
	}))
	return ts
}

var oauthStorage *TestStorage

func newOauthServer() *osin.Server {
	oauthStorage = NewTestStorage()
	client := &osin.DefaultClient{
		Id:          "AUTH0_CLIENT_ID",
		Secret:      "AUTH0_CLIENT_SECRET",
		RedirectUri: "AUTH0_CALLBACK_URL",
	}
	oauthStorage.SetClient("AUTH0_CLIENT_ID", client)
	oauthStorage.SaveAuthorize(&osin.AuthorizeData{
		Code:        existingUserWithoutTokenCode,
		Client:      client,
		CreatedAt:   time.Now(),
		ExpiresIn:   10000,
		RedirectUri: "AUTH0_CALLBACK_URL",
		UserData:    existingUserWithoutToken,
	})
	oauthStorage.SaveAuthorize(&osin.AuthorizeData{
		Code:        existingUserWithTokenCode,
		Client:      client,
		CreatedAt:   time.Now(),
		ExpiresIn:   10000,
		RedirectUri: "AUTH0_CALLBACK_URL",
		UserData:    existingUserWithToken,
	})

	config := osin.NewServerConfig()
	config.AllowedAuthorizeTypes = osin.AllowedAuthorizeType{osin.CODE, osin.TOKEN}
	config.AllowedAccessTypes = osin.AllowedAccessType{osin.AUTHORIZATION_CODE, osin.REFRESH_TOKEN,
		osin.PASSWORD, osin.CLIENT_CREDENTIALS, osin.ASSERTION}
	config.AllowGetAccessRequest = true
	config.AllowClientSecretInParams = true

	oauthServer := osin.NewServer(config, oauthStorage)
	return oauthServer
}
