package mock

import "gitlab.com/j-velasco/s3gatekeeper/pkg"

func NewUserStub(email string) UserStub {
	return UserStub{email}
}

func NewExchangeServiceStub() *fakeExchangeService {
	return &fakeExchangeService{
		responses: make(map[pkg.OAuthCode]struct {
			pkg.User
			error
		}),
	}
}

type UserStub struct {
	email string
}

func (us UserStub) Email() string {
	return us.email
}

type fakeExchangeService struct {
	responses map[pkg.OAuthCode]struct {
		pkg.User
		error
	}
}

func (fs *fakeExchangeService) ExchangeCode(code pkg.OAuthCode) (pkg.User, error) {
	return fs.responses[code].User, fs.responses[code].error
}

func (fs *fakeExchangeService) WhenCode(c pkg.OAuthCode, u pkg.User, err error) {
	fs.responses[c] = struct {
		pkg.User
		error
	}{u, err}
}
